# JD's NixOS Builds

## Description

This repo stores my ongoing work to understand Nix/ NixOS.  I'm still learning, so this will most likely not be a good reference for some time.

## Support
- [Mobile-NixOS](https://mobile.nixos.org/)
- [NixOS on ARM](https://matrix.to/#/#nixos-on-arm:nixos.org) Matrix chat

## Roadmap
1. Figure out core installations
2. Figure out device specific installation configs

## Authors and acknowledgment
- samueldr & Mindavi for answering so many of my questions

## Project status
In-progess, but not recommended as a good starting point, yet.

## Usage
* Install NixOS.
* Temporarily install git:
  * `nix-shell -p nixos.git`
* Clone this repo
  * `git clone https://gitlab.com/jgonyea/nixos.git ~/jgonyea-nixos`
* Modify the top `configuration.nix` file import line for the appropriate host (todo: I need to figure out a way of making this automatic)
* Rsync the files.  Note, this will override the current `configuration.nix`:
  * `sudo rsync -ravh . /etc/nixos`
* Change directory:
  * `cd /etc/nixos`
* Perform a nixos-rebuild:
  * `sudo nixos-rebuild switch`
* Reboot

At this point, all items should be ready to go.

## Devices currently being worked on
- Pine64 Pinebook Pro
- Pine64 Pinephone (3GB RAM model)
- x86_64 Build (Blackbird)
- Qemu VM build