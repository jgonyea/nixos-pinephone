{ config, lib, pkgs, ... }:

{
    imports = [
        /etc/nixos/config/desktop.nix
        /etc/nixos/config/virtualization.nix
    ];

    #
    # System related items.
    #
    # Bootloader.
    boot.loader.grub.enable = true;
    boot.loader.grub.device = "/dev/sda";
    boot.loader.grub.useOSProber = true;
    boot.supportedFilesystems = [ "ntfs" ];
    #boot.kernelPackages = pkgs.linuxPackages_latest;

    networking.hostName = "blackbird-nix";

    # nVidia 2080 drivers.
    # Make sure opengl is enabled
    hardware.opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };

    # NVIDIA drivers are unfree.
    nixpkgs.config.allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
        "nvidia-x11"
      ];

    # Tell Xorg to use the nvidia driver
    services.xserver.videoDrivers = ["nvidia"];

    hardware.nvidia = {

      # Modesetting is needed for most wayland compositors
      modesetting.enable = true;

      # Use the open source version of the kernel module
      # Only available on driver 515.43.04+
      open = true;

      # Enable the nvidia settings menu
      nvidiaSettings = true;

      # Optionally, you may need to select the appropriate driver version for your specific GPU.
      package = config.boot.kernelPackages.nvidiaPackages.stable;
    };

    #
    # User related items.
    #
    users.users.jgonyea.packages = with pkgs; [
        cura
        element-desktop
        lutris
        prismlauncher
        protontricks
        retroarch
        scanmem
        signal-desktop
    ];
    programs.steam.enable = true;

}
