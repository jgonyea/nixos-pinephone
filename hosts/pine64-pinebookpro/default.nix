{ config, lib, pkgs, ... }:

{
    imports = [
        /etc/nixos/config/desktop.nix
        /etc/nixos/config/virtualization.nix
    ];

    #
    # System related items.
    #
    # Use the systemd-boot EFI boot loader.
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = false;

    networking.hostName = "glider";

    #
    # User related items.
    #
    users.users.jgonyea.packages = with pkgs; [
        element-desktop
        #lutris
        prismlauncher
        #retroarch
        #scanmem
        #signal-desktop
    ];

}
