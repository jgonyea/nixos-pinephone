{ config, lib, pkgs, ... }:

{
  imports = [
    (import <mobile-nixos/lib/configuration.nix> { device = "pine64-pinephone"; })
    ./hardware-configuration.nix
    <mobile-nixos/examples/phosh/phosh.nix>
  ];

  networking.hostName = "quadcopter";

  #
  # Opinionated defaults
  #
  
  # Use PulseAudio
  hardware.pulseaudio.enable = true;
  
  # Enable Bluetooth
  hardware.bluetooth.enable = true;
  
  # Bluetooth audio
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  
  # Enable power management options
  powerManagement.enable = true;
  
  # It's recommended to keep enabled on these constrained devices
  zramSwap.enable = true;

  # Auto-login for phosh
  services.xserver.desktopManager.phosh = {
    user = "jgonyea";
  };

  #
  # System configuration
  # 
environment.systemPackages = with pkgs; [
	curl
	vim
	wget
];
services.openssh.enable = true;

  #
  # User configuration
  #
  
  users.users."jgonyea" = {
    isNormalUser = true;
    description = "jgonyea";
    extraGroups = [
      "dialout"
      "feedbackd"
      "networkmanager"
      "video"
      "wheel"
    ];
    packages = with pkgs; [
	    firefox
    ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}