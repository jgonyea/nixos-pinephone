{ config, lib, pkgs, ... }:

{
  #
  # System related items.
  #

  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";
  boot.loader.grub.useOSProber = true;


  

}