{config, pkgs, ...}:

{
    users.users.jgonyea = {
        isNormalUser = true;
        description = "jgonyea";
        extraGroups = [
            "networkmanager"
            "wheel"
        ];
        openssh.authorizedKeys.keys = [
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDY/ekirAnhyfSjARZ3DOTZ4zX6bYtes7sgEq1+iTNevo3EAc7AltHWng3G8v50+xm+EUqg4obeaNCsY6eEL0/Mrisvelp1k2CIYDa06bcNlyzv3xhaj40D++AMkyI5TyZCXL6PbKIn79edSCEpmlfG8TRJgB9k3mL21cUT9nfpoRAHfHAxJLhJ1Z8f7ahRDoDhGhj38HuUh5PqsajUDMbNaGfyPA8ONwHyrLyu8B0whajdRNF5d2ymIQ4l5Ej70JKchPFgfugpNhJKnCsKpJDDlUFWptAf//7811qATTUk+0qrHR+cN54hCYCqzbsH29oyo8fMWvo6SnW7wqpvXosH7LfmFvMw/VjgA+qRGHB4SzM6c8Hl4cK803fz3eY9vfFS3mcf0QolrD1JhZvDVZrcE2rXUkv/YYyJimJ+x7Wq0SHHi7bzVyeaa9rLXFgBe9KpBDcYGhKasRSFRygfNSrQmrMkrM8//IXS8lZmqfdC9PstCD77GwVJMp2bL9niLRej2A8E+MnJhjCv90nlvfQB0cirGNIpHE5+QofnwJCiygojV5jeRxHmlqSwHiUFrmNnzXZMKJheo9mY0QcaD3CxauJroK5LdkeFnKOxAKAsNL88PqqK4jK1PvLGpEcqeS3V/X+FwM1i9mtQZZHViJwFm3LxjX564bv22nbPRzCl9Q== rsa202112"
        ];
        packages = with pkgs; [
            calibre
            firefox
            nextcloud-client
            thunderbird
            zsh
        ];
  };
}
