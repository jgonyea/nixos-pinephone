{ config, pkgs, ... }:

{
    virtualisation = {
        docker.enable = true;
        libvirtd.enable = true;
    };

    environment.systemPackages = with pkgs; [
        virt-manager
    ];

    programs.dconf.enable = true;

    users.users.jgonyea.extraGroups = [
        "docker"
        "libvirtd"
    ];
}
